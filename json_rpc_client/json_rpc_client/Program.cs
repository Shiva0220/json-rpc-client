﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace json_rpc_client
{
    internal class Program
    {
        private const string Version = "2.0.5";
        private static List<string> _methodsCalled = new List<string>();
        private static TcpClient _tcpClient;
        private static Socket _socket;
        private static NetworkStream _networkStream;
        private static string message;

        public static void Main(string[] args)
        {
            var ip = "";
            if (args.Length == 0)
            {
                Console.WriteLine(">> [ERR] IP address of the server not provided in arguments! Setting IP to 127.0.0.1");
                ip = "127.0.0.1";
            }
            else
            {
                ip = args[0];
            }

            //tcp client init
            _initClient(ip);

            //start comm
            _startComm();
        }

        private static void _initClient(string ip)
        {
            try
            {
                ip = ip.Contains(".") ? ip : $"192.168.5.{int.Parse(ip)}";
                _tcpClient = new TcpClient(AddressFamily.InterNetwork);
                Console.WriteLine($">>> Establishing a connection with {ip} at port 1869...");
                _tcpClient.Connect(ip, 1869);
                Console.WriteLine(">>> Connection established.");
                Console.WriteLine($"*** This version of client is equivalent to {Version} version of payment server. Check the release notes of {Version} to get the specifications of this client ***");
                _networkStream = _tcpClient.GetStream();
            }
            catch (FormatException e)
            {
                Console.WriteLine(">> [ERR] " + e.Message);
                Console.WriteLine(">> Enter the ip address again");
                var ip1 = Console.ReadLine();
                _initClient(ip1);
            }
        }

        private static void _startComm()
        {
            while (true)
            {
                try
                {
//                    Console.WriteLine($"Current message value: {message}");
                    Console.WriteLine(@">> Select action:
0. pre-auth
1. confirm
2. void
3. transaction status
4. transaction details
5. select device
6. get device init status
7. upload logs to s3
8. get connected devices
l. Last message
List. list of methods called till now");
                    var keyRead = Console.Read();
                    _getMessage(Encoding.ASCII.GetString(new byte[] {Convert.ToByte(keyRead)}), out message);
                    if (string.IsNullOrEmpty(message))
                    {
                        continue;
                    }

                    Console.WriteLine(">> Sending request...");
                    var sendBytes = Encoding.ASCII.GetBytes(message);
                    _networkStream.Write(sendBytes, 0, sendBytes.Length);
                    Console.WriteLine(">>> Request sent: " + message);

                    var responseArrived = false;
                    var bytesReceived = new byte[1024];
                    while (!responseArrived)
                    {
                        var readBytes = _networkStream.Read(bytesReceived, 0, bytesReceived.Length);
                        if (readBytes > 0)
                        {
                            Console.WriteLine(
                                $">>> Response received: {Encoding.ASCII.GetString(bytesReceived.Take(readBytes).ToArray())}");
                            responseArrived = true;
                        }
                    }

//                    Console.WriteLine($"Message value at the end of last request: {message}");
                }
                catch (Exception)
                {
                }
            }
        }

        private static void _getMessage(string key, out string message)
        {
            switch (key)
            {
                //pre-auth
                case "0":
                    message =
                        $"{{\"id\":1,\"method\":\"preAuth\",\"params\": {{\"input1\": {{\"coupon\": {{\"code\": \"YIDQ5791\", \"discount\": 20.0, \"discountType\": \"percentage\", \"endDate\": null, \"expiry\": \"Never\", \"issuedAt\": \"Analysis Portal\", \"redeemableAt\": \"All\", \"redeemableAtTimestamp\": \"All:1583029972085\", \"redeemed\": false, \"startDate\": null, \"type\": \"coupon\"}}, \"currency\": \"USD\", \"discount\": 140, \"priceAfterTax\": 606, \"priceBeforeTax\": {_getAmount()}, \"taxCharged\": 1, \"taxPercentage\": 8.25, \"totalDisplay\": \"before_tax\"}},\"input2\":840,\"input3\":60}}}}\n";
                    break;
                //confirm
                case "1":
                    message = 
                        $"{{\"id\":1,\"method\":\"confirm\",\"params\": {{\"input1\": {{\"coupon\": {{\"code\": \"YIDQ5791\", \"discount\": 20.0, \"discountType\": \"percentage\", \"endDate\": null, \"expiry\": \"Never\", \"issuedAt\": \"Analysis Portal\", \"redeemableAt\": \"All\", \"redeemableAtTimestamp\": \"All:1583029972085\", \"redeemed\": false, \"startDate\": null, \"type\": \"coupon\"}}, \"currency\": \"USD\", \"discount\": 140, \"priceAfterTax\": 606, \"priceBeforeTax\": {_getAmount()}, \"taxCharged\": 1, \"taxPercentage\": 8.25, \"totalDisplay\": \"before_tax\"}}}}}}\n";
                    break;
                //void
                case "2":
                    message = "{\"jsonrpc\": \"2.0\", \"method\": \"authVoid\", \"params\": [], \"id\": 1}\n";
                    break;
                //transaction status
                case "3":
                    message = "{\"jsonrpc\": \"2.0\", \"method\": \"getTransStatus\", \"params\": [], \"id\": 1}\n";
                    break;
                //transaction details
                case "4":
                    message =
                        "{\"jsonrpc\": \"2.0\", \"method\": \"getTransactionDetails\", \"params\": [], \"id\": 1}\n";
                    break;
                //select device
                case "5":
                    Console.WriteLine(@"Select device id:
0. VERIFONE
1. OTI
2. USA TECH
3. APRIVA BARCODE SCANNER
4. ONETOUCH
5. NOVO
6. GEMPAY
");
                    var device = Console.Read().ToString();
                    switch (Encoding.ASCII.GetString(new byte[] {Convert.ToByte(device)}))
                    {
                        case "0":
                            device = "VERIFONE";
                            break;
                        case "1":
                            device = "OTI";
                            break;
                        case "2":
                            device = "USATECH";
                            break;
                        case "3":
                            device = "APRIVABARCODESCANNER";
                            break;
                        case "4":
                            device = "ONETOUCH";
                            break;
                        case "5":
                            device = "NOVO";
                            break;
                        case "6":
                            device = "GEMPAY";
                            break;
                        default:
                            device = "OTI";
                            break;
                    }
                    

                    message =
                        $"{{\"jsonrpc\": \"2.0\", \"method\": \"selectDevice\", \"params\": [\"{device}\"], \"id\": 1}}\n";
                    break;
                case "6":
                    message =
                        $"{{\"jsonrpc\": \"2.0\", \"method\": \"getDeviceStatus\", \"params\": [], \"id\": 1}}\n";
                    break;
                case "7":
                    Console.WriteLine("$$ Enter start date:");
                    var startDate = Console.ReadLine();
                    Console.WriteLine("$$ Enter end date:");
                    var endDate = Console.ReadLine();
                    Console.WriteLine("## Enter the sally id:");
                    var sallyId = Console.ReadLine();
                    message =
                        $"{{\"jsonrpc\": \"2.0\", \"method\": \"uploadLogs\", \"params\": [\"{startDate}\", \"{endDate}\", \"{sallyId}\"], \"id\": 1}}\n";
                    break;
                case "8":
                    message =
                        $"{{\"jsonrpc\": \"2.0\", \"method\": \"getConnectedDevices\", \"params\": {{\"defaultSallyData\":{{\"SallyID\": \"Sally186\", \"CardReaders\": [\"USATECH\", \"OTI\", \"APRIVABARCODESCANNER\", \"ONETOUCH\", \"NOVO\", \"GEMPAY\"]}}}}, \"id\": 1}}\n";
                    break;
                //list all methods called till now
                case "l":
                case "L":
                    message = Program.message;
                    Console.WriteLine($">> Message: {message}");
                    break;
                case "List":
                    Console.WriteLine("Methods Called: ");
                    for (var i = 0; i < _methodsCalled.Count; i++)
                    {
                        Console.WriteLine($"{i}. {_methodsCalled[i]}");
                    }

                    message = "";
                    break;
                default:
                    Console.WriteLine($"key: {key}");
                    message = "";
                    break;
            }
        }

        private static decimal _getAmount()
        {
            Console.WriteLine();
            Console.WriteLine("## Enter pre-auth amount: ");
            var keys = Console.ReadLine();
            try
            {
                return decimal.Parse(keys);
            }
            catch (Exception e)
            {
                Console.WriteLine($">> [ERR] Exception occured!! Error message: {e.Message}");
                return _getAmount();
            }
        }
    }
}